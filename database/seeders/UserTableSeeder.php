<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;
use Hash;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $exists = User::where('email','admin@mail.com')->first();
        if(!$exists){
            $user = User::create([
                'name' => 'Admin',
                'email' => 'admin@mail.com',
                'email_verified_at' => date('Y-m-d H:i:s'),
                'password' => Hash::make('123456789')
            ]);

            $user->assignRole('Admin');
        }

        $exists = User::where('email','user@mail.com')->first();
        if(!$exists){
            $user = User::create([
                'name' => 'User',
                'email' => 'user@mail.com',
                'email_verified_at' => date('Y-m-d H:i:s'),
                'password' => Hash::make('123456789')
            ]);

            $user->assignRole('User');
        }
    }
}
