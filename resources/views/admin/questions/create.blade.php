<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <div class="flash-message">
                        @if($errors)
                            @foreach($errors->all() as $error)
                                <div class="text-danger">{{$error}}</div>
                            @endforeach
                        @endif
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                          @if(Session::has($msg))
                          <p class="alert alert-{{ $msg }}">{{ Session::get($msg) }}</p>
                          @endif
                        @endforeach
                      </div>

                    <form id="question-form" method="POST" action="{{route('questions.store')}}">
                        @csrf
                        <div class="form-group">
                          <label for="exampleFormControlInput1">Question Name</label>
                          <input type="name" class="form-control" id="name" name='name' placeholder="Question">
                        </div>
                        <br>
                        @foreach($options as $key => $option)
                            <div class="form-group mb-2">
                                <label for="">Option{{$loop->iteration}}</label>
                                <div class="row">
                                    <div class="col-md-8">
                                        <input type="name" class="form-control" name="options[{{$key}}]" placeholder="Option{{$loop->iteration}}">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="radio" name="is_correct" value="{{$key}}" > Correct
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <div class="form-group mb-2 mt-2">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {!! JsValidator::formRequest('App\Http\Requests\StoreQuestionRequest', '#question-form') !!}
</x-app-layout>
