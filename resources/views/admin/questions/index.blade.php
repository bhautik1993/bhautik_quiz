<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                          @if(Session::has($msg))
                          <p class="alert alert-{{ $msg }}">{{ Session::get($msg) }}</p>
                          @endif
                        @endforeach
                      </div>
                    <div class="text-right mb-1">
                        <a href="{{route('questions.create')}}" class="btn btn-primary ">Add Question</a>
                    </div>
                    <table class="table">
                        <thead class="table-dark">
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Question</th>
                            <th scope="col">Options</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($questionDB as $question)
                                <tr>
                                    <th scope="row">{{$question->id}}</th>
                                    <td>{{$question->name}}</td>
                                    <td>
                                        <ul>
                                        @foreach($question->options as $option)
                                        <li>{{$loop->iteration." - ".$option->option}}</li>
                                        @endforeach
                                        </ul>
                                    </td>
                                    <td>
                                        <a href="{{route('questions.edit',$question->id)}}" class="btn btn-primary btn-sm">Edit</a>

                                        <form action="{{route('questions.destroy', $question->id)}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                      </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
