<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                          @if(Session::has($msg))
                          <p class="alert alert-{{ $msg }}">{{ Session::get($msg) }}</p>
                          @endif
                        @endforeach
                      </div>
                   
                    <table class="table">
                        <thead class="table-dark">
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">User Name</th>
                            <th scope="col">Time (In Minutes)</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($userDB as $user)
                                <tr>
                                    <th scope="row">{{$user->id}}</th>
                                    <td>{{$user->name}}</td>
                                    <td>
                                        @if($user->answerLog && $user->answerLog->started_at && $user->answerLog->ended_at)
                                        
                                            @php
                                                $startDate = \Carbon\Carbon::parse($user->answerLog->started_at);
                                                $endDate = \Carbon\Carbon::parse($user->answerLog->ended_at);
                                                $diffInMinutes = $endDate->diffInMinutes($startDate);
                                            @endphp

                                            {{$diffInMinutes}}
                                        @else
                                            Not Attempt
                                        @endif
                                    </td>
                                    <td>
                                        <a href="" class="btn btn-primary btn-sm">View</a>

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                      </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
