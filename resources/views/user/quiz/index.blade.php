<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

<style>
* {
    margin: 0;
    padding: 0;
}

html {
    height: 100%;
}

/*Background color*/
#grad1 {
    background-color: : #9C27B0;
    background-image: linear-gradient(120deg, #FF4081, #81D4FA);
}

/*form styles*/
#msform {
    text-align: center;
    position: relative;
    margin-top: 20px;
}

#msform fieldset .form-card {
    background: white;
    border: 0 none;
    border-radius: 0px;
    box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.2);
    padding: 20px 40px 30px 40px;
    box-sizing: border-box;
    width: 94%;
    margin: 0 3% 20px 3%;

    /*stacking fieldsets above each other*/
    position: relative;
}


/*Hide all except first fieldset*/
#msform fieldset:not(:first-of-type) {
    display: none;
}

#msform fieldset .form-card {
    text-align: left;
    color: #9E9E9E;
}

/*Blue Buttons*/
#msform .action-button {
    width: 100px;
    background: skyblue;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px;
}

#msform .action-button:hover, #msform .action-button:focus {
    box-shadow: 0 0 0 2px white, 0 0 0 3px skyblue;
}

/*Previous Buttons*/
#msform .action-button-previous {
    width: 100px;
    background: #616161;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px;
}

#msform .action-button-previous:hover, #msform .action-button-previous:focus {
    box-shadow: 0 0 0 2px white, 0 0 0 3px #616161;
}

/*Dropdown List Exp Date*/
select.list-dt {
    border: none;
    outline: 0;
    border-bottom: 1px solid #ccc;
    padding: 2px 5px 3px 5px;
    margin: 2px;
}

select.list-dt:focus {
    border-bottom: 2px solid skyblue;
}

/*The background card*/
.card {
    z-index: 0;
    border: none;
    border-radius: 0.5rem;
    position: relative;
}

/*FieldSet headings*/
.fs-title {
    font-size: 25px;
    color: #2C3E50;
    margin-bottom: 10px;
    font-weight: bold;
    text-align: left;
}

/*progressbar*/
#progressbar {
    margin-bottom: 30px;
    overflow: hidden;
    color: lightgrey;
}

#progressbar .active {
    color: #000000;
}

#progressbar li {
    list-style-type: none;
    font-size: 12px;
    width: 25%;
    float: left;
    position: relative;
}

/*Icons in the ProgressBar*/
#progressbar #account:before {
    font-family: FontAwesome;
    content: "\f023";
}

#progressbar #personal:before {
    font-family: FontAwesome;
    content: "\f007";
}

#progressbar #payment:before {
    font-family: FontAwesome;
    content: "\f09d";
}

#progressbar #confirm:before {
    font-family: FontAwesome;
    content: "\f00c";
}

/*ProgressBar before any progress*/
#progressbar li:before {
    width: 50px;
    height: 50px;
    line-height: 45px;
    display: block;
    font-size: 18px;
    color: #ffffff;
    background: lightgray;
    border-radius: 50%;
    margin: 0 auto 10px auto;
    padding: 2px;
}

/*ProgressBar connectors*/
#progressbar li:after {
    content: '';
    width: 100%;
    height: 2px;
    background: lightgray;
    position: absolute;
    left: 0;
    top: 25px;
    z-index: -1;
}

/*Color number of the step and the connector before it*/
#progressbar li.active:before, #progressbar li.active:after {
    background: skyblue;
}

/*Imaged Radio Buttons*/
.radio-group {
    position: relative;
    margin-bottom: 25px;
}

.radio {
    display:inline-block;
    width: 204;
    height: 104;
    border-radius: 0;
    background: lightblue;
    box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.2);
    box-sizing: border-box;
    cursor:pointer;
    margin: 8px 2px; 
}

.radio:hover {
    box-shadow: 2px 2px 2px 2px rgba(0, 0, 0, 0.3);
}

.radio.selected {
    box-shadow: 1px 1px 2px 2px rgba(0, 0, 0, 0.1);
}

/*Fit image in bootstrap div*/
.fit-image{
    width: 100%;
    object-fit: cover;
}

</style>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                          @if(Session::has($msg))
                          <p class="alert alert-{{ $msg }}">{{ Session::get($msg) }}</p>
                          @endif
                        @endforeach
                    </div>
                    <div class="container-fluid" id="grad1">
                        <div class="row justify-content-center mt-0">
                            <div class="col-11 col-sm-9 col-md-7 col-lg-6 text-center p-0 mt-3 mb-2">
                                <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                                    @php
                                        $answerLog = Auth::user()->answerLog;
                                        $answers = Auth::user()->answers;
                                    @endphp
                                    @if($answerLog)
                                        @if($answerLog->ended_at)
                                            <h2><strong>Result</strong></h2>
                                            @php
                                                $startDate = \Carbon\Carbon::parse($answerLog->started_at);
                                                $endDate = \Carbon\Carbon::parse($answerLog->ended_at);
                                                $diffInMinutes = $endDate->diffInMinutes($startDate);
                                            @endphp
                                            <p>Total Time: {{$diffInMinutes}} (In Minutes)</p>
                                            <p>Correct Answer: {{$answers->where('is_correct', '1')->count()}} Out of {{$answers->count()}}</p>
                                        @else
                                        <div id="quizResponse">
                                            <h2><strong>Select Options</strong></h2>
                                            <div class="row">
                                                <div class="col-md-12 mx-0">
                                                    <form id="msform" action="{{route('quiz.store')}}" method="POST">
                                                        @csrf
                                                    @foreach($questionDB as $question)
                                                        <fieldset>
                                                            <div class="form-card">
                                                                <h2 class="fs-title">{{$question->name}}</h2>
                                                                @foreach($question->options as $key => $option)
                                                                <div class="radio-group">
                                                                    <input type="radio" class="radio answerOption" name="answers[{{$question->id}}]" value="{{$option->id}}"/> {{$option->option}}
                                                                </div>
                                                                @endforeach
                                                                <span class="error-msg text-danger"></span>
                                                            </div>
                                                            <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                                                            
                                                            @if($loop->last)
                                                                <input type="submit" name="Submit" class="next action-button" value="Submit"/>
                                                            @else
                                                                <input type="button" name="next" class="next action-button" value="Next Step"/>
                                                            @endif
                                                        </fieldset>
                                                        @endforeach
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    @else
                                        <h2><strong>Start Quiz</strong></h2>
                                        <p>All question are required.</p>
                                        <div class="row">
                                            <div class="col-md-12 mx-0">
                                                <form id="" action={{route('quiz.start')}} method="POST">
                                                    @csrf
                                                    <button class="btn btn-success">Start Quiz</button>
                                                </form>
                                            </div>
                                        </div>
                                    @endif
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>
$(document).ready(function(){
    
    var current_fs, next_fs, previous_fs; //fieldsets
    var opacity;
    
    $('.answerOption').change(function(){
        $(this).parent().find('.error-msg').html('');
    });

    $(".next").click(function(){
        var isChecked = false;
        current_fs = $(this).parent();
        next_fs = $(this).parent().next();
        radioInputs = current_fs.find("input[type='radio']");
        
        radioInputs.each(function(){
            if($(this).prop("checked")){
                isChecked = true;
                return false;
            }
        });        

        if(isChecked){
            //show the next fieldset
            next_fs.show(); 
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function(now) {
                    // for making fielset appear animation
                    opacity = 1 - now;
        
                    current_fs.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    next_fs.css({'opacity': opacity});
                }, 
                duration: 600
            });
        } else{
            current_fs.find('.error-msg').html('Please choose at least one option');
        }
        
    });
    
    $(".previous").click(function(){
        
        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();
        
        //Remove class active
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
        
        //show the previous fieldset
        previous_fs.show();
    
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now) {
                // for making fielset appear animation
                opacity = 1 - now;
    
                current_fs.css({
                    'display': 'none',
                    'position': 'relative'
                });
                previous_fs.css({'opacity': opacity});
            }, 
            duration: 600
        });
    });
    
    $('.radio-group .radio').click(function(){
        $(this).parent().find('.radio').removeClass('selected');
        $(this).addClass('selected');
    });
    
    $("#msform").submit(function(event){
        event.preventDefault();
        var formData = $(this).serialize();

        $.ajax({
            url: "{{route('quiz.store')}}",
            type: "POST",
            data: formData,
            success: function(response){
                $("#quizResponse").html('<p>Total time: '+response.minutes+' (in Minutes)</p><p>Correct Answer: '+response.correctAnswer+'</p>');
            },
            error: function(xhr){
                alert("Something went wrong!!");
            }
        });
    })
        
    });
    </script>
</x-app-layout>
