<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\StoreQuestionRequest;
use App\Models\Question;
use App\Models\Option;
use DB;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $questionDB = Question::orderBy('id','desc')->get();

        return view('admin.questions.index', compact('questionDB'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $options = ['option1','option2','option3','option4'];
        return view('admin.questions.create', compact('options'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreQuestionRequest $request)
    {
        //dd($request->all());
        DB::beginTransaction();
        try{
            $questionDB = Question::create(['name' => $request->name]);
            foreach($request->options as $key => $option){
                $optionData = [
                    'question_id' => $questionDB->id,
                    'option' => $option,
                    'is_correct' => ($key == $request->is_correct) ? '1' :'0'
                ];
    
                Option::create($optionData);

                DB::commit();
            }

            return redirect()->route('questions.index')->with('success', 'Question added successfully.');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('questions.create')->with('danger', 'Somethin went wrong. Please try again.');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $questionDB = Question::with('options')->findOrFail($id);
        return view('admin.questions.edit', compact('questionDB'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreQuestionRequest $request, string $id)
    {
        DB::beginTransaction();
        try{
            $questionDB = Question::updateOrCreate(['id'=>$id],['name' => $request->name]);
            foreach($request->options as $key => $option){
                $optionData = [
                    'option' => $option,
                    'is_correct' => ($key == $request->is_correct) ? '1' :'0'
                ];
    
                Option::updateOrCreate(['id' => $key], $optionData);

                DB::commit();
            }

            return redirect()->route('questions.index')->with('success', 'Question updated successfully.');
        } catch (\Exception $e) {
            DB::rollback();
            return back()->with('danger', 'Somethin went wrong. Please try again.');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $questionDB = Question::findOrFail($id);
        $questionDB->delete();
        return redirect()->route('questions.index')->with('success', 'Question deleted successfully.');
    }
}
