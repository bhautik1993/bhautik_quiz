<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Question;
use App\Models\User;
use DB;

class UserController extends Controller
{
    public function index(){
        $userDB = User::whereHas('roles', function($q){
            return $q->where('name', 'User');
        })->orderBy('id','DESC')->get();

        return view('admin.users.index', compact('userDB'));
    }
}
