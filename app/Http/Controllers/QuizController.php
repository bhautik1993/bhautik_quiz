<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Question;
use App\Models\AnswerLog;
use App\Models\Answer;
use App\Models\Option;
use Auth;
use Carbon\Carbon;

class QuizController extends Controller
{
    public function index(){
        $questionDB = Question::all(); 
        
        return view('user.quiz.index', compact('questionDB'));
    }

    public function start(Request $request){
        AnswerLog::create([
            'user_id' => Auth::user()->id,
            'started_at' => Carbon::now()
        ]);

        return redirect()->route('quiz.index');
    }
    public function store(Request $request)
    {
       // dd($request->answers);
        foreach($request->answers as $key => $answer){
            $question = Question::with(['options' => function($query){
                return $query->where('is_correct','1');
            }])
            ->where('id', $key)->first();

            $option = Option::find($answer);

            $data = [
                'question_id' => $key,
                'option_id' => $answer,
                'user_id' => Auth::user()->id,
                'question_name' => $question->name,
                'option_name' => $option->option,
                'is_correct' =>($question->options[0]->id == $answer) ? '1' : '0'
            ];

            $answer = Answer::updateOrCreate(['question_id' => $key, 'user_id' => Auth::user()->id], $data);
        }

        $totalCorrectAnswers = Answer::where('user_id', Auth::user()->id)->where('is_correct', '1')->count();
        $answerLog = AnswerLog::where('user_id', Auth::user()->id)->first();
        $answerLog->ended_at = Carbon::now();
        $answerLog->save();

        $startDate = $answerLog->started_at;
        $endDate = $answerLog->ended_at;
        $diffInMinutes = $endDate->diffInMinutes($startDate);

        return response()->json(['message' => 'Answer saved successfully', 'minutes' => $diffInMinutes, 'correctAnswer' => $totalCorrectAnswers]);
        // dd($request->all());
    }
}
