<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class AuthController extends Controller
{
    public function login(Request $request){
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            $user = Auth::user();

            if(!$user->hasRole('User')){
                Auth::logout();
                return response()->json(['success' => 0, 'message'=>'Unauthorized'], 401);
            }
            $token = $user->createToken('api-auth')->plainTextToken;

            return response()->json([
                'success' => 1,
                'token' => $token,
                'user' => $user
            ]);

        }

        return response()->json(['success' => 0, 'message'=>'Unauthorized'], 401);
    }
}
