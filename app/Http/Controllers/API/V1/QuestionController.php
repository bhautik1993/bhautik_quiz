<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Question;
use App\Models\AnswerLog;
use App\Models\Option;
use App\Models\Answer;
use Auth;
use Carbon\Carbon;

class QuestionController extends Controller
{
    public function getQuestions(Request $request){
        $answerLog = Auth::user()->answerLog;
        $questionDB = Question::with('options')->get()->toArray();

        $responseArr = [
            'questions' => $questionDB,
            'answerLog' => $answerLog
        ];

        return response()->json([
            'success' => 1,
            'message' => 'Success',
            'data' => $responseArr
        ]);
    }

    public function startQuiz(Request $request){
        $answerLog = Auth::user()->answerLog;
        if($answerLog){
            if(!is_null($answerLog->ended_at)){
                return response()->json([
                    'success' => 0,
                    'message' => 'You have already attempt this quiz',
                    'data' => []
                ]);
            }
            return response()->json([
                'success' => 0,
                'message' => 'Quiz already started',
                'data' => []
            ]);
        } else{
            AnswerLog::create([
                'user_id' => Auth::user()->id,
                'started_at' => Carbon::now()
            ]);
        }

        return response()->json([
            'success' => 1,
            'message' => 'Quiz started successfully.',
            'data' => []
        ]);
    }

    public function submitQuiz(Request $request){
        try{
            $answerLog = Auth::user()->answerLog;
            if($answerLog){
                if(!is_null($answerLog->ended_at)){
                    return response()->json([
                        'success' => 0,
                        'message' => 'You have already attempt this quiz',
                        'data' => []
                    ]);
                }

                foreach($request->answer as $key => $answer){
                    $question = Question::with(['options' => function($query){
                        return $query->where('is_correct','1');
                    }])
                    ->where('id', $key)->first();
        
                    $option = Option::find($answer);
        
                    $data = [
                        'question_id' => $key,
                        'option_id' => $answer,
                        'user_id' => Auth::user()->id,
                        'question_name' => $question->name,
                        'option_name' => $option->option,
                        'is_correct' =>($question->options[0]->id == $answer) ? '1' : '0'
                    ];
        
                    $answer = Answer::updateOrCreate(['question_id' => $key, 'user_id' => Auth::user()->id], $data);
                }

                $totalCorrectAnswers = Answer::where('user_id', Auth::user()->id)->where('is_correct', '1')->count();
                $answerLog = AnswerLog::where('user_id', Auth::user()->id)->first();
                $answerLog->ended_at = Carbon::now();
                $answerLog->save();
        
                $startDate = $answerLog->started_at;
                $endDate = $answerLog->ended_at;
                $diffInMinutes = $endDate->diffInMinutes($startDate);
        
                $responseArr = [
                    'minutes' => $diffInMinutes,
                    'correctAnswer' => $totalCorrectAnswers
                ];
                return response()->json([
                    'success' => 1,
                    'message' => 'Quiz submitted successfully.',
                    'data' => $responseArr
                ]);

            } else{
                return response()->json([
                    'success' => 0,
                    'message' => 'Something went wrong',
                    'data' => []
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'success' => 0,
                'message' => $e->getMessage(),
                'data' => []
            ]);
        }
    }
}
